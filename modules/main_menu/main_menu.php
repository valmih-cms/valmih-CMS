<?php

class main_menu extends Module {
	function install() {}

	function getMenuItems($jsonPath) {
		$str = file_get_contents($jsonPath);
		$menuArray = json_decode($str, true);

		function processOne($menuArray) {
			$res = [];
			foreach ($menuArray as $k => $v) {
				$item = ["name" => $k];
				$item["children"] = [];
				if (isset($v["href"])) {
					$item["href"] = Constants::$webPath . "/" . $v["href"];
				}

				if (isset($v["children"])) {
					$item["children"] = processOne($v["children"]);
				}
				$res[] = $item;
			}
			return $res;
		}
		$res = processOne($menuArray);
		return $res;
	}

}