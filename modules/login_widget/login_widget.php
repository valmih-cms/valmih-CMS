<?php

class login_widget extends Module {
	function __construct($connection, $modulePath) {
		parent::__construct($connection, $modulePath);

	}

	function install() {}

	function getWidget() {
		$page = Page::getInstance("", ["login"]);
		$loginModule = $page->getModule("login");

		echo "<div class=\"loginWidget\">";

		if($loginModule->isLogged()) {
			echo "Hello $_SESSION[user] <br/>";
			echo <<<EOT
				<form method="post">
					<button type="submit" name="logout" class="button">Logout</button>
				</form>
			EOT;
		}
		else {
			echo <<<EOT
				<form method="post">
					Username: <input type="text" name="user"> <br/>
					Password: <input type="password" name="password"> <br/>
					<button type="submit" name="login" class="button">Login</button>
				</form>
			EOT;
		}
	}
}

?>