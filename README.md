# ValMih CMS 2.0
Content Management System (CMS) for Web applications written in PHP. Version 2.0.

Project is currently being rewritten to use better modularization and abstractions.

## Prerequisites

### 1. Web server setup

In order to use the CMS, you need to install a web server. Currently Apache2 and Nginx are supported. Then, one must update the settings of the web server in order to allow rewrite rules, which are essential to the CMS (for routing and DB installing)

#### 1.1 Apache2

Rewrite rules are enabled through `apache2.conf` configuration file. Under Linux this can be done like this:
```
sudo vim /etc/apache2/apache2.conf
```
Then, in vim, insert a new entry like this. Note that the path can be different.
```
<Directory /var/www/html/valmih-CMS>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
```

Change the root directory from `/var/www/html` to `/var/www/html/valmih-CMS`.
```
sudo vim /etc/apache2/sites-available/000-default.conf
```

And `DocumentRoot /var/www/html` -> `DocumentRoot /var/www/html/valmih-CMS`

Enable mdoule `RewriteEngine`.
```
sudo a2enmod rewrite
```

Finally, restart the web server:
```
sudo systemctl restart apache2
```

#### 1.2 Nginx
TODO

### 2. PHP setup

The CMS is written in PHP, so you need to install the scripting language in the system. The CMS works by storing various information in a SQL Database. Two SQL implementations are supported: MySQL and SQLite, both of which are supported directly in PHP, however they must be activated.

#### Sqlite3

For `sqlite3` database, install `php-sqlite3` package. Then, edit `/etc/php/X/apache2/php.ini` file, where X is the version of PHP (7.3 for example). Uncomment (remove the `#`) the lines `#extension=pdo_sqlite` and `#extension=sqlite3`.

#### MySQL

For `MySQL`, edit the `/etc/php/X/apache2/php.ini` file. Uncomment the line `extension=mysqli`. 
 
Then, restart the web server and you are done!

## Initial setup

### 3. Installing the database

Make the db directory writable

```
mkdir /var/www/html/valmih-CMS/db
chmod 7777 -R /var/www/html/vamih-CMS/db
```

### 4. Installing modules

TODO (stub below)

```
$page = Page::getInstance("Page");
$modulePath = Constants::$modulesPath . "/module_name";
echo $modulePath;
require_once("$modulePath/module_name.php");
$a = new module_name($page->connection, $modulePath);

$a->install();
$page->connection->insertInto("settings", ["category", "name", "information"],
     ["modules", "module_name", "active"]);

```
