<?php
$page = Page::getInstance("CMS :: Login Page", ["login", "main_menu"]);
$thisModule = $page->getModule("login");
include_once("menu.php");

if(isset($_POST["login"])) {
	if(!$thisModule->tryLogin($_POST["user"], $_POST["password"])) {
		redirectWithMessage("login", 3, "Wrong username or password!");
		exit;
	}
	redirect("index");
}

if(isset($_GET["logout"])) {
	if($thisModule->isLogged()) {
		$thisModule->logout();
	};
	redirect("login");
}

/* If already logged in, then just redirect to index. */
if($thisModule->isLogged()) {
	redirect("index");
}

$page->getHeader();
?>

<div class="content">
	<div class="divLoginForm">
		<h1>Login page</h1>
		<form method="post">
		    Username: <input type="text" name="user"> <br/>
		    Password: <input type="password" name="password"> <br/>
		    <button type="submit" name="login" class="button">Login</button>
		</form>
	</div>
</div>

<?php
$page->getFooter();
?>
