<?php

class SQLiteConnection extends Connection {
	function __construct() {
		$this->connection = new SQLite3(SQLiteConnection::getDBPath());
	}

	function query($queryStr) {
		$result = $this->connection->query($queryStr);
		$errorMsg = "Query: $queryStr. Error: " . $this->getError();
		assert ($result !== NULL, $errorMsg);
		return new SQLiteResult($result);
	}

	function close() {
		$this->connection->close();
	}

	function getError() {
		return $this->connection->lastErrorMsg();
	}

	function getLastInsertedId() {
		$res = $this->query("SELECT last_insert_rowid()");
		return $res->fetchArray("num")[0][0];
	}

	function createTable($name, $arrNames, $types, $firstRowAutoIncrement=true, $firstRowPrimaryKey=true) {
		assert (count($arrNames) == count($types));
		$query = "CREATE TABLE IF NOT EXISTS `$name` (";

		$query .= "`$arrNames[0]` $types[0]";
		if($firstRowPrimaryKey) {
			$query .= " primary key";
		}
		if($firstRowAutoIncrement) {
			$query .= " autoincrement";
		}

		$n = count($arrNames);
		for ($i=1; $i<$n; $i++) {
			$query .= ", `$arrNames[$i]` $types[$i]";
		}
		$query .= ")";
		return $this->connection->query($query);
	}

	static function getDBPath() {
		return Constants::$absolutePath . "/" . SQLITE_DB_PATH;
	}

	static function deleteDB() {
		if(is_file(SQLiteConnection::getDBPath())) {
			unlink(SQLiteConnection::getDBPath());
		}
	}

	/* Function that creates a new database, called from install.php. */
	static function createDB() {
		try {
			$connection = new SQLite3(SQLiteConnection::getDBPath(), SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
		}
		catch (Exception $e) {
			echo $e;
			return null;
		}
		return new SQLiteConnection();
	}

	/* Function that checks that the database was installed properly. */
	static function checkDB() {
		try {
			$connection = new SQLite3(SQLiteConnection::getDBPath(), SQLITE3_OPEN_READONLY);
		}
		catch (Exception $e) {
			return false;
		}

		$query = "SELECT name FROM sqlite_master WHERE type='table' AND name='users'";
		$result = $connection->query($query);
		if ($result->fetchArray() == false) {
			return false;
		}

		$query = "SELECT name FROM sqlite_master WHERE type='table' AND name='settings'";
		$result = $connection->query($query);
		if ($result->fetchArray() == false) {
			return false;
		}

		return true;
	}

}

?>