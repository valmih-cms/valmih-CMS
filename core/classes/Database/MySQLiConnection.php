<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class MySQLiConnection extends Connection {
	function __construct() {
		$this->connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		if ($this->connection->connect_errno)
			throw new Exception("Unable to connect to the database!");

		//safety utf8
		$this->query("SET NAMES utf8");
	}

	function query($queryStr) {
		$result = $this->connection->query($queryStr);
		assert ($result !== NULL, new Exception($this->getError()));
		return new MySQLiResult($result);
	}

	function close() {
		$this->connection->close();
	}

	function getError() {
		return $this->connection->error;
	}

	function getLastInsertedId() {
		assert (false, "TODO");
	}

	/* Function that creates a new database, called from install.php. */
	static function createDB() {
		$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD);
		if (!$connection)
			return false;

		$query = "CREATE DATABASE IF NOT EXISTS `" . DB_NAME . "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
		$result = $connection->query($query);
		$connection->close();
		if (!$result) {
			return false;
		}

		/* Connect to new created DB and create the basic tables settings and users. */
		$connection = new MySQLiConnection();
		$connection->query("CREATE TABLE IF NOT EXISTS `settings` (`id` int NOT NULL AUTO_INCREMENT, PRIMARY KEY(id),
			`category` varchar(50) NOT NULL, `name` varchar(50) NOT NULL, `value` varchar(100) NOT NULL,
			`information` varchar(256) NOT NULL)");
		$connection->query("CREATE TABLE IF NOT EXISTS `users` (`id` int NOT NULL AUTO_INCREMENT, PRIMARY KEY(id),
			`username` varchar(32) NOT NULL, `password` varchar(32) NOT NULL)");
		$connection->install();

		$connection->close();
		return true;
	}

	//functie ce verifica existenta bazei de date
	static function checkDB() {
		try {
			$connection = new MySQLiConnection();
		}
		catch (Exception $e) {
			return false;
		}

		$query = "SHOW TABLES LIKE 'users'";
		$result = $connection->query($query);
		if($result->numRows() == 0) {
			return false;
		}

		$query = "SHOW TABLES LIKE 'settings'";
		$result = $connection->query($query);
		if($result->numRows() == 0) {
			return false;
		}

		return true;
	}

}

?>