<?php

abstract class DBResult {
	function __construct($result) {
		if ($result == NULL) {
			throw new Exception("Result was NULL");
		}

		$this->result = $result;
	}

	abstract function fetchArray($type);
	abstract function numRows();
}

?>