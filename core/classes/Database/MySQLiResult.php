<?php

class MySQLiResult extends DBResult {
	static $types = [
		"num" => MYSQLI_NUM,
		"assoc" => MYSQLI_ASSOC,
	];
	
	function fetchArray($type) {
		$res = [];
		while ($item = $this->result->fetch_array(MySQLiResult::$types[$type])) {
			$res[] = $item;
		}
		return $res;
	}

	function numRows() {
		return $this->result->num_rows;
	}

}

?>