<?php

class SQLiteResult extends DBResult {
	static $types = [
		"num" => SQLITE3_NUM,
		"assoc" => SQLITE3_ASSOC,
	];
	
	function fetchArray($type) {
		$res = [];
		while ($item = $this->result->fetchArray(SQLiteResult::$types[$type])) {
			$res[] = $item;
		}
		return $res;
	}

	function numRows() {
		return $this->numRows();
	}
}

?>