<?php
/**
 * Router.php
 *
 * A class that defines a router which loads the designated page according to the routes file.
 * The link is parsed based on Constants::$requestLink which is set by the requester (browser).
 */
class Router {

	private $pageFile;

	function __construct() {
		$this->pageFile = $this->readRoutes();
	}

	/* Return the requested route to be loaded by loadPage() after parsing of special characters */
	function parseRoute($route) {
		/* TODO: GET/POST stuff */
		/* TODO: somehow be able to route to other routes (index => blog, with blog => $app/blog_index.php */
		$route = str_replace("\$application", realpath("../application"), $route);
		$route = str_replace("\$modules", realpath("../modules"), $route);
		$route = str_replace("\$core", realpath("../core"), $route);

		/* Assume that insead of a file, we are given a path to a module and try to load index.php */
		if(!endsWith($route, ".php") && !is_file($route)) {
			$route = $route . "/index.php";
		}

		return $route;
	}

	/* TODO: more complicated key parsing like page/%d => page?id=%d or $_POST["id"]=%d */
	function parseKey($key) {
		return $key;
	}

	function getRoutesPathFile() {
		assert(Constants::$databaseClass::checkDB());

		$connection = new Constants::$databaseClass;
		$routesPath = $connection->getInfo("value", "settings", "category='routesPath'", "first_row, first_value");
		$routesPath = Constants::$webPath . "/$routesPath";
		$connection->close();
		assert (is_file($routesPath));
		return $routesPath;
	}

	function readRoutes() {
		try {
			$routesPath = $this->getRoutesPathFile();
		} catch (Exception $e) {
			echo "Database not installed. No routes can be read yet. <br/>";
			return "install.php";
		}
		$handle = fopen($routesPath, "r");
		assert ($handle != null, "Error opening the routes file!");

		$found = null;
		while (($line = fgets($handle)) !== false) {
			$line = removeWhiteSpace($line);
			if(empty($line) || startsWith($line, "#")){
				continue;
			}

			$array = explode("=>", $line);
			if(count($array) != 2 || empty($array[0]) || empty($array[1])) {
				echo "Warning: Malformed route line: <b>$line</b>. Skipping. <br/>";
				continue;
			}

			$thisKey = $this->parseKey($array[0]);
			$thisValue = $this->parseRoute($array[1]);

			if($thisKey == Constants::$requestLink) {
				$found = $thisValue;
				break;
			}

			/* If we meet the default, set it in case we meet an invalid route */
			if($thisKey === "*") {
				$found = $thisValue;
			}
		}
		
		fclose($handle);
		assert ($found != null, "No route was found");
		return $found;
	}

	function loadPage() {
		assert(!empty($this->pageFile), "Empty redirect link.");
		assert (file_exists($this->pageFile) && is_readable($this->pageFile),
			"Warning: Desired routed page file '" . $this->pageFile . "' does not exist or is not readable!");
		require_once($this->pageFile);
	}

}
?>