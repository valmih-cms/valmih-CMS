<?php
/**
 * Span.php
 *
 * A class that defines a generic section of the HTML Page. A section can be some simple text, a <div>, a <span>, etc.
 */
class Section {

	protected $body = "";

	function addSection(Section $section) {
		$this->body .= $section;
	}

	function addText($text) {
		$this->body .= $text;
	}

	/* Fun fact: $text could be a Section too, because of weak typing. So it is possible to have a call like:
	 * addURLText("www.google.ro", (new Section())->composeStyledText("Fancy message", fancyCssClass));
	 */
	function addURLText($url, $text) {
		$this->addText("<a href='$url'>$text</a>");
	}

	function addNewLine() {
		$this->addText("<br>");
	}

	function addStyledText($text, $styleClass="") {
		$this->addSection((new Span($styleClass))->composeText($text));
	}

	/* Generic input tag, all possible types are described here: http://www.w3schools.com/tags/att_input_type.asp */
	/* attributes has 2 purposes: one it could work for a text field to add disabled.
	 * second purpose is to add specific type attributes, that are not common for all inputs, like min and max for 
	 * input type number, or alt for input type image.
	 * Example: addInput("image", "", "", "someCssClass", "src='picture.png' alt='Poza' width='48' height='48'");
	 */
	function addInput($type, $name="", $value="", $styleClass="", $attributes="") {
		$this->body .= "<input type='$type' name='$name' value='$value' class='$styleClass' $attributes />";
	}

	function addTextField($name="", $value="", $styleClass="", $attributes="") {
		$this->addInput("text", $name, $value, $styleClass, $attributes);
	}

	function addPasswordField($name="", $value="", $styleClass="", $attributes="") {
		$this->addInput("password", $name, $value, $styleClass, $attributes);
	}

	function addTextArea($name="", $rows=0, $cols=0, $styleClass="", $attributes="") {
		$this->body .= "<textarea name='$name' rows='$rows' cols='$cols' class='$styleClass' $attributes></textarea>";
	}

	function addSubmitButton($name="", $value="", $styleClass="", $attributes="") {
		$this->addInput("submit", $name, $value, $styleClass, $attributes);
	}

	function addSelect($name, array $options) {
		$this->body .= "<select name='$name'>";
		
		foreach ($options as $option)
			$this->body .= "<option>$option</option>";
		
		$this->body .= "</select>";
	}

	function addImage($url, $alt="") {
		$this->body .= "<img src='$url' alt='$alt' />";
	}

	/* Magic functions.
	 * __call is used to implement the fluent interface pattern based on the existing methods
	 * __toString is used to concatenate this section to a potentinal other section, or to print the section's html
	 */
	function __call($functionName, array $arguments) {
		/* composeText -> call addText and returns "this" */
		if (startsWith($functionName, "compose")) {
			$newFunctionName = "add".substr($functionName, strlen("compose"));
			call_user_func_array(array($this, $newFunctionName), $arguments);
			
			return $this;
		}
		else if (startsWith($functionName, "concat")) {
			$newFunctionName = "add".substr($functionName, strlen("concat"));
			call_user_func_array(array($this, $newFunctionName), $arguments);

			return $this;
		}
		else if ($functionName === "build" || $functionName === "show")
			echo $this;
		else
			throw new Exception("Wrong function name!");
	}

	function __toString() {
		return $this->body;
	}

}
?>
