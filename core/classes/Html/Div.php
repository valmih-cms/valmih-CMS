<?php
/**
 * Div.php
 *
 * A class that defines a <div> HTML element. A Div is considered a section of the web page.
 */
class Div extends Section {

	private $styleClass;

	function __construct($styleClass = "") {
		$this->styleClass = $styleClass;
	}

	function __toString() {
		return "<div class='$this->styleClass'>$this->body</div>";
	}

}
?>
