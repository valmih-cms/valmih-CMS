<?php
/**
 * Span.php
 *
 * A class that defines a <span> HTML element. A Span is considered a section of the web page.
 */
class Span extends Section {

	private $styleClass;

	function __construct($styleClass = "") {
		$this->styleClass = $styleClass;
	}

	function __toString() {
		return "<span class='$this->styleClass'>$this->body</span>";
	}

}
?>
