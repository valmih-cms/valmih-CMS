<?php
/**
 * Form.php
 *
 * A class that defines a <form> HTML element. A Form is considered a section of the web page.
 */
class Form extends Section {

	private $method;
	private $action;
	private $styleClass;

	function __construct($method, $action, $styleClass="") {
		$this->method = $method;
		$this->action = $action;
		$this->styleClass = $styleClass;
	}

	function __toString() {
		return "<form method='$this->method' action='$this->action' class='$this->styleClass'>$this->body</form>";
	}

}
?>
