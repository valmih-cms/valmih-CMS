<?php
/**
 * Table.php
 *
 * A class that defines a <table> HTML element. A Table is considered a section of the web page.
 */
class Table extends Section {

	private $styleClass;
	private $cellspacing;

	function __construct($styleClass = "", $cellspacing = 15) {
		$this->styleClass = $styleClass;
		$this->cellspacing = $cellspacing;
	}
	
	function startRow() {
		parent::addText("<tr>");
	}

	function endRow() {
		parent::addText("</tr>");
	}

	function newRow() {
		$this->endRow();
		$this->startRow();
	}

	function addSection(Section $section) {
		parent::addText("<td>");
		parent::addSection($section);
		parent::addText("</td>");
	}

	function addText($text) {
		parent::addText("<td>".$text."</td>");
	}

	function addStyledText($text, $styleClass="") {
		parent::addText("<td>");
		parent::addStyledText($text, $styleClass);
		parent::addText("</td>");
	}

	/* De remarcat, cand se face addTextField, pe un obiect Table, desi acea metoda e doar in Section, ea e mostenita
	 * ca atare, deci cand face this->addInput, va apela addInput de aici, din Table, apoi pe cel din Section.
	 */
	function addInput($type, $name="", $value="", $styleClass="", $attributes="") {
		parent::addText("<td>");
		parent::addInput($type, $name, $value, $styleClass, $attributes);
		parent::addText("</td>");
	}

	function addTextArea($name="", $rows="0", $cols="0", $styleClass="", $attributes="") {
		parent::addText("<td>");
		parent::addTextArea($name, $rows, $cols, $styleClass, $attributes);
		parent::addText("</td>");
	}

	function addSelect($name, array $options) {
		parent::addText("<td>");
		parent::addSelect($name, $options);
		parent::addText("</td>");
	}

	function addImage($url, $alt="") { 
		parent::addText("<td>");
		parent::addImage($url, $alt);
		parent::addText("</td>");
	}

	function __toString() {
		return "<table class='$this->styleClass' cellspacing='$this->cellspacing'>$this->body</table>";
	}

}
?>
