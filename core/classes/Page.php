<?php
/**
 * Page.php
 *
 * A core class that defines a Web Page. A page has a HTML structure, a DB connection, a theme, plugins 
 * and loaded libraries.
 */
class Page {

	private static $instance = null;
	public $connection;
	private $title;
	private $modules;
	private $theme;

	private $headerShown, $footerShown;

	/*
	 * Page constructor.
	 * @param[in] title The title of the page that appears on the top of the browser.
	 * @param[in] enabledModules A list of modules that are to be enabled, or "*" to load all active ones. If a
	 *  requested module is inactive or inexistent, a warning will be thrown.
	 */
	private function __construct($title, $enabledModules) {
		$this->connection = new Constants::$databaseClass;
		$this->setTitle($title);
		$this->modules = $this->loadActiveModules($enabledModules);
		$this->theme = $this->loadTheme();
		$this->headerShown = false;
		$this->footerShown = false;
	}

	/* Page is a singleton so we don't instantiate it for no reason multiple times in same page. */
	public static function getInstance($title="", $enabledModules=[]) {
		if(Page::$instance == null) {
			Page::$instance = new Page($title, $enabledModules);
		}
		else {
			/* If a page is already instanciated, use that. */
			assert($title == "" && $enabledModules == []);
		}
		return Page::$instance;
	}

	function loadActiveModules($enabledModules) {
		/* Load all active modules. If a list is given (and not "*") as parameter, then load only required ones and
		* check that all of them are active and present. */
		$dbModules = $this->connection->getInfo("name", "settings", "category='modules' AND information='active'");
		$result = [];
		foreach ($dbModules as $dbModule) {
			$strModule = $dbModule["name"];

			$modulePath = Constants::$modulesPath . "/$strModule";
			require_once("$modulePath/$strModule.php");

			/* Store the module's name and path in the result array. */
			$isRequired = is_array($enabledModules) && in_array($strModule, $enabledModules);
			if($enabledModules === "*" || $isRequired) {
				$result[$strModule] = $modulePath;
			}
		}

		if($enabledModules !== "*") {
			$arrayDiff = array_diff($enabledModules, array_keys($result));
			if(count($arrayDiff) > 0) {
				echo "Warning. Not all required modules were enabled: ";
			}
		}

		/* Instantiate all modules. TODO: topological sort if needed. */
		foreach ($result as $strModule=>$modulePath) {
			$result[$strModule] = new $strModule($this->connection, $modulePath);
		}

		return $result;
	}

	function loadTheme() {
		$themePath = Constants::$absolutePath . "/themes";
		$siteTheme = $this->connection->getInfo("value", "settings", "category='siteTheme'", "first_row, first_value");
		require_once("$themePath/$siteTheme/$siteTheme.php");
		$theme = new $siteTheme();
		$theme->getHead();
		return $theme;
	}

	function getHeader() {
		if($this->headerShown) {
			return;
		}

		$this->theme->getHeader();
		$this->headerShown = true;
	}

	function getFooter() {
		if($this->footerShown) {
			return;
		}

		/* Safety divs for closing page well if needed. */
		echo "</div> </div> </div> </div> </div> </div> </div> </div> </div> </div>";
		$this->theme->getFooter();
		$this->footerShown = true;
	}

	function setTitle($title) {
		$this->title = $title;
		echo <<<EOF
		<title> $title </title>
		EOF;
	}

	function getTitle() {
		return $this->title;
	}

	function getModule($module) {
		if(!in_array($module, array_keys($this->modules))) {
			echo "Module $module is not loaded. <br/>";
			$this->getFooter();
			exit;
		}

		return $this->modules[$module];
	}

	function __destruct() {
		$this->connection->close();
	}
}
?>
