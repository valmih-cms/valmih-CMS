<?php

abstract class Theme {
	/* Site structure should be:
		<head> includes and such </head>
		<body>
			<header> logo </header>
			<main> content loaded by the page's source </main>
			<footer> footer message </footer>
		</body>
	*/

	abstract function getHead();
	abstract function getHeader();
	abstract function getFooter();
}

?>