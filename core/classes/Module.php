<?php
/**
 * Module.php
 *
 * Abstract class that defines a Module of the CMS. Every Module should extend this class and
 *  implement its abstract functions.
 */
abstract class Module {
	public $connection;

	function __construct($connection, $modulePath) {
		$this->connection = $connection;
		$this->modulePath = $modulePath;
	}

	function isEnabled() {
		return true;
	}

	function setEnabled($isEnabled) {
	}

	function getName() {
		return "";
	}

	abstract function install();
}
?>
