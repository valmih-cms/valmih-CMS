<?php
/**
 * Constants.php
 *
 * Constants that are used throughout the CMS by the core API and the user code.
 */

class Constants {

	public static $baseDirectory, $absolutePath, $modulesPath, $applicationPath,
		$webPath, $requestLink, $modulesWebPath;
	public static $themePath;
	public static $pluginPath, $pluginThemePath;
	public static $databaseClass, $db;

	static function initialize() {
		self::$baseDirectory = self::getBaseDirectory();
		self::$absolutePath = self::getAbsolutePath();
		self::$modulesPath = self::getModulesPath();
		self::$applicationPath = self::getApplicationPath();
		self::$webPath = self::getWebPath();
		self::$modulesWebPath = self::getModulesWebPath();
		self::$requestLink = self::getRequestLink();
		self::$databaseClass = [
			"MySQLi" => "MySQLiConnection",
			"SQLite" => "SQLiteConnection"
		][DB_TYPE];
	}

	static function initializeDBConstants() {
		self::$db = new Constants::$databaseClass;
		self::$themePath = self::getThemePath();
	}

	/* This is based on the relative path from CMS.php and the data in $_SERVER["SCRIPT_NAME"].
	 * If the CMS.php file is located at /A/B/C/Site/core/CMS.php, the site nmae is /A/B/C/Site.
	 * There's no need to store this information in DB, if we can use CMS.php as base file.
	 */
	private static function getBaseDirectory() {
		$scriptName = $_SERVER["SCRIPT_NAME"];
		$pos = strpos($scriptName, "/core/CMS.php");

		if ($pos === false)
			throw new Exception("Can not initialize base directory!");

		$temp = substr($_SERVER["REQUEST_URI"], 0, $pos);
		/* Remove leading '/' */
		return substr($temp, 1);
	}

	private static function getAbsolutePath() {
		$root = "$_SERVER[DOCUMENT_ROOT]";
		$baseDirectory = Constants::getBaseDirectory();
		if(!empty($baseDirectory)) {
			$root = "$root/$baseDirectory";
		}
		return $root;
	}

	private static function getApplicationPath() {
		return Constants::getAbsolutePath() . "/application";
	}

	private static function getModulesPath() {
		return Constants::getAbsolutePath() . "/modules";
	}

	private static function getWebPath() {		
		$root = "http://$_SERVER[HTTP_HOST]";
		$baseDirectory = Constants::getBaseDirectory();
		if(!empty($baseDirectory)) {
			$root = "$root/$baseDirectory";
		}
		return $root;
	}

	private static function getModulesWebPath() {
		return self::$webPath . "/modules";
	}

	private static function getRequestLink() {
		function parseRequestLink($link) {
			if (empty(Constants::$baseDirectory))
				$link = substr($link, 1);
			else
				$link = substr($link, strlen(Constants::$baseDirectory) + 2);

			/* Remove training '/' if existing. */
			if(strlen($link) > 1 && endsWith($link, "/")) {
				$link = substr($link, 0, -1);
			}
			return $link;
		}

		$link = "";
		if (empty($_GET))
			$link = $_SERVER["REQUEST_URI"];
		else {
			/* Find where the GET parameters start */
			$pos = strpos($_SERVER["REQUEST_URI"], "?");
			if($pos === false)
				$link = $_SERVER["REQUEST_URI"];
			else {
				$link = substr($_SERVER["REQUEST_URI"], 0, $pos);
			}
		}

		return parseRequestLink($link);
	}

	private static function getThemePath() {
		$siteTheme = (self::$db)->getInfo("value", "settings", "category='siteTheme'", "first_row, first_value");
		return self::$webPath . "/themes/" . $siteTheme;
	}

	static function printConstants() {
		echo "[General]";
		echo "<br/> - CMS Version: " . CMS_VERSION;
		echo "<br/> - Base Directory: " . self::$baseDirectory;
		echo "<br/> - Absolute Path: " . self::$absolutePath;
		echo "<br/> - Modules Path: " . self::$modulesPath;
		echo "<br/> - Web Path: " . self::$webPath;
		echo "<br/> - Web Path: " . self::$modulesWebPath;
		echo "<br/> Request Link: " . self::$requestLink;
		echo "<br/> [DB]";
		echo "<br/> - Type: " . self::$databaseClass;
		echo "<br/> - Connection: " . (self::$db != NULL);
		if (self::$db != NULL) {
			echo "<br/> - Path: " . (self::$db)->getDbPath();
			echo "<br/> - Theme: " . self::$themePath;
		}
	}

}
?>
