<?php
/**
 * functions.php
 *
 * Basic PHP functions used by the CMS.
 */


function includeCSS($filePath) {
	?> <link href="<?php echo $filePath; ?>" rel="stylesheet" type="text/css" /> <?php
}

function includeJS($filePath) {
	?> <script type="text/javascript" charset="utf-8" src="<?php echo $filePath; ?>"></script> <?php
}

/* "a, b, c, asdf" => array("a", "b", "c", "asdf") */
function strToArray($str, $parsingToken = " ,") {
    if (is_array($str)) {
		return $str;
    }
    
    if (is_numeric($str)) {
        return [$str];
    }

    assert (is_string($str));
	$arr = array();
	$tok = strtok($str, $parsingToken);
	while($tok !== false){
		$arr[] = $tok;
		$tok = strtok($parsingToken);
	}

	return $arr;
}

function startsWith($haystack, $needle) {
	/* search backwards starting from haystack length characters from the end */
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
	/* search forward starting from end minus needle length characters */
	return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
}

function removeWhiteSpace($str) {
	return preg_replace('/\s+/', '', $str);
}

function refresh() {
	header("Location: $_SERVER[REQUEST_URI]");
	exit;
}

function redirect($url, $time=0) {
	header("refresh: $time; url=" . Constants::$webPath . "/$url");
	if($time == 0) {
		exit;
	}
}

function redirectWithMessage($url, $time, $message) {
	assert ($time > 0);
	$page = Page::getInstance();
	$page->getHeader();

	echo($message);
	$page->getFooter();

	redirect($url, $time);
	exit;
}

function prepareStringForDB($string) {
	$string = str_replace("'", "''", $string);
	return $string;
}

function removeHtmlTagAndContent($input) {
	return preg_replace("(<([a-z]+)>.*?</\\1>)is", "", $input);
}

?>
