<?php
/**
 * settings.php
 *
 * Configuration settings for the current site.
 *   STATUS (the status of the site) -> OFFLINE, TEST, ONLINE
 *   DB_* (database parameters)
 *   MAIL_* (mail parameters)
 */
define("STATUS", "OFFLINE");

define("DB_TYPE", "SQLite");
define("SQLITE_DB_PATH", "db/valmih_CMS.db");
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "valmih_CMS");

define("MAIL_HOST", "localhost");
define("MAIL_USER", "TODO");
define("MAIL_PASSWORD", "");
define("MAIL_ALIAS", "TODO");

define("CMS_VERSION", "2.0.0");
Constants::initialize();

?>
