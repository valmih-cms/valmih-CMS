<?php
/**
 * CMS.php
 *
 * The core PHP file of the CMS which should be included by every file of the user's application in order to benefit
 * from the features offered by the CMS.
 */

function classLoader($class) {
	$validPaths = ["classes", "classes/Database", "classes/Html"];
	$found = False;
	foreach ($validPaths as $path) {
		if(file_exists("$path/$class.php")) {
			$found = True;
			require_once("$path/$class.php");
		}
	}
	if (!$found) {
		throw new Exception ("$class.php does not exist in any path");
	}
}

/* Enable asserts as errors */
function assert_handle(string $file , int $line , string $assertion, string $description) {
	echo "Assert fail: $assertion<br/>";
	echo "File: $file. Line: $line<br/>";
	echo "Description: $description<br/>";
	throw new Exception();
}

function exception_error_handler($errno, $errstr, $errfile, $errline ) {
	echo "Error fail: $errstr (code: $errno) <br/>";
	echo "File: $errfile. Line: $errline <br/>";
	throw new Exception();
	// throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}

function doRouting() {
	/* check if database exists */
	$dbClass = Constants::$databaseClass;
	assert ($dbClass != NULL);
	if (!$dbClass::checkDB()) {
		if (Constants::$requestLink !== "install") {
			$path = Constants::$webPath . "/install";
			echo "No database installed, use the <a href='$path'>install</a> page.";
			exit;
		}
		else {
			/* load the installation page */
			(new Router())->loadPage();
		}
	}
	else {
		if (Constants::$requestLink === "install") {
			echo "Database alredy exists! (" . DB_NAME . ")";
			redirect("index", 3);
			exit;
		}
		else {
			/* database exists, so we can initialize the database constants and load the requested page */
			Constants::initializeDBConstants();
			(new Router())->loadPage();
		}
	}
}

# Do the page setup stuff.
ob_start();
session_start();
spl_autoload_register("classLoader");
require_once("functions.php");
require_once("settings.php");
set_error_handler("exception_error_handler");
assert_options(ASSERT_CALLBACK, "assert_handle");

# Finally, do the page routing
doRouting();

?>
