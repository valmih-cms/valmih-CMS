<?php

if(isset($_POST["submitButton"])) {
	/* databaseClass is defined in Constants.php and represents the type of Database we're using. */
	$dbClass = Constants::$databaseClass;

	/* We first call the static method createDB(), which must make the necessarily setup in order to instal the basic
	 * database tables. */
	try {
		$connection = $dbClass::createDB();
		$dbClass::createInitialTables($connection);
	}
	catch (Exception $e) {
		echo $e;
		$dbClass::deleteDB();
		echo("Error creating initial tables. Retrying.");
		redirect("install", 3);
		exit;
	}

	echo("Database installed succesful!");
	redirect("index", 3);
	exit;
}
?>

<h1> CMS install page </h1>

<form method="post" action="install">
	<h3/> Database name (config/settings.php) </h3>
		<input type="text" name="databaseName" value=<?php echo DB_NAME; ?> disabled> <br/>

	<!-- Accounts stuff -->
	<h3> Admin account </h3>
	<input type="text" name="adminUsername"> <br/>
	<input type="password" name="adminPassword"> <br/>

	<!-- Modules Stuff -->
	<h3> Modules </h3>
	<?php $modules = array_diff(scandir(Constants::$modulesPath), array("..", ".")); ?>
	<ul>
	<?php foreach ($modules as $module) {
		echo "<li> $module <input type=\"checkbox\" name=\"checkBoxModule_$module\" value=\"$module\" checked> </li>";
	} ?>
	</ul>

	<!-- Themes Stuff -->
	<h3> Theme </h4>
	<?php
	$themesPath = Constants::$absolutePath . "/themes";
	$themes = array_diff(scandir($themesPath), array("..", "."));
	$usableThemes = [];

	foreach($themes as $theme) {
		$expectedThemeFile = "$themesPath/$theme/$theme.php";
		if(!file_exists($expectedThemeFile)) {
			echo "Warning! Theme directory '$theme' exists, but no '$expectedThemeFile' file. Skipping. <br/>";
			continue;
		}
		$usableThemes[] = $theme;
	}
	?>
	<select name="selectedTheme">
		<?php foreach($usableThemes as $theme) {
			echo "<option value=\"$theme\"> $theme </option>";
		}
	?>
	</select>

	<!-- Install button -->
	<br/></br>
	<button name="submitButton"> Install </button>
</form>