/* Create a top message and move it to contentLeft so it's shown on the left side of the content */
function updateMenu() {
	$(".menu").prepend("<div class=\"menuTop\"><a href=\"index\">Main Menu</a></div>");
	$(".menu").addClass("sideBarBox").appendTo(".contentLeft").css({ "display": "block" });
}

function updateLoginWidget() {
	$(".loginWidget").addClass("sideBarBox").prependTo(".contentRight")
		.css({ "display": "block", "text-align" : "center", "padding" : "0.5em", "min-height" : "0em" });
}

function runWithJQuery(jQueryCode){
	if(window.jQuery) {
		jQueryCode();
	}
	else {
		var script = document.createElement("script");
		document.head.appendChild(script);
		script.type = "text/javascript";
		script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
		script.onload = jQueryCode;
	}
}

runWithJQuery(function jQueryCode(){
	updateLoginWidget();
	updateMenu();
});